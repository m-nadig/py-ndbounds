# ndbounds

Toolbox for handling n-dimensional bounds.

The basic idea of the NdBounds format is to combine methods for working with boundaries in
* 1D (e.g. onset & end of waveforms),
* 2D (e.g. bounding boxes in an image) or
* any higher nD case.

## Install

The toolbox is available on PyPI: `pip install ndbounds`

## Usage

```python
import ndbounds

# Create 3 BBoxes
xy = [[20, 30], [180, 110], [0.7, 44.6]]  # starts
w = [[10, 10], [100, 100], [0.2, 0.1]]  # widths
bounds = ndbounds.as_bounds(start=xy, width=w)

# Modify their position
bounds_mod = bounds.shift_center([0.5, 1000])

n_dims = bounds.get_n_dims()
print(f'{n_dims}D-BBox original center:\n{bounds.get_center()}\n')
print(f'{n_dims}D-BBox modified center:\n{bounds_mod.get_center()}')
```
