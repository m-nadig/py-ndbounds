
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import cv2
import ndbounds


# Bboxes in the format (x1, y1, x2, y2)
bboxes = [
    [100, 100, 200, 300],
    # [250, 150, 400, 270],
]
print('Original bboxes:\n{}'.format(bboxes))

# Create NdBounds object
# bboxes = np.reshape(bboxes, (-1, 2, 2)).transpose((0, 2, 1))
# b1 = ndbounds.interpret_as_se(bboxes)
b1 = ndbounds.interpret_as_s1s2e1e2(bboxes)

# Some 2D waveform as example image
shape_original = (600, 400)
(width, height) = shape_original
xs = np.linspace(-2*np.pi, 2*np.pi, width)
ys = np.linspace(-2*np.pi, 2*np.pi, height)
tau, phi = np.meshgrid(xs, ys)
img = np.sin(tau+phi)

# Modify width using a function
b2 = b1.apply_func_on_width(np.log)#, inplace=True)  # .apply_func_on_width(np.exp)

# Resize the image and get resized Bboxes
factor_resize = (0.5, 0.5)
shape_resize = tuple(np.round(np.multiply(factor_resize, shape_original)).astype('int'))
img_resized = cv2.resize(img, shape_resize)
b3 = b1.scale_dimensions(factor_resize)

# Modify offset
b4 = b1.shift_center((250, 50))


cmap = plt.get_cmap('summer')

fig, ax = plt.subplots(2, 2)
ax = ax.flatten()

linewidth = 2
for axes in ax:
    axes.grid(linestyle='dashed')

ax[0].set_title('Original Bboxes')
ax[0].imshow(img, cmap=cmap)
for (x, y), (w, h) in zip(b1.get_start(), b1.get_width()):
    patch = patches.Rectangle((x, y), w, h, edgecolor='red', facecolor='none', linewidth=linewidth)
    ax[0].add_patch(patch)

ax[1].set_title('Modified Bboxes\n(function on width)')
ax[1].imshow(img, cmap=cmap)
for (x, y), (w, h) in zip(b2.get_start(), b2.get_width()):
    patch = patches.Rectangle((x, y), w, h, edgecolor='red', facecolor='none', linewidth=linewidth)
    ax[1].add_patch(patch)

ax[2].set_title('Resized')
ax[2].imshow(img_resized, cmap=cmap)
ax[2].set_xlim([0, width])
ax[2].set_ylim([height, 0])
for (x, y), (w, h) in zip(b3.get_start(), b3.get_width()):
    patch = patches.Rectangle((x, y), w, h, edgecolor='red', facecolor='none', linewidth=linewidth)
    ax[2].add_patch(patch)

ax[3].set_title('Modified Bboxes\n(offset)')
ax[3].imshow(img, cmap=cmap)
for (x, y), (w, h) in zip(b4.get_start(), b4.get_width()):
    patch = patches.Rectangle((x, y), w, h, edgecolor='red', facecolor='none', linewidth=linewidth)
    ax[3].add_patch(patch)

plt.show()
