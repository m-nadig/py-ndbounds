
import numpy as np
import ndbounds


output = [
    [[[1, 5]],
     [[2, 9]]],
    [[[-3, 8]],
     [[-4, 4]]],
]

output = np.array(output).astype('float')

b1 = ndbounds.interpret_as_se(output)
print(type(b1._bound_handler), b1.get_start().tolist())
b1.require_start()
print(type(b1._bound_handler), b1.get_start().tolist())
b1.convert_to_cw(inplace=True)
print(type(b1._bound_handler), b1.get_start().tolist())
b1.require_start()
print(type(b1._bound_handler), b1.get_start().tolist(), b1.get_end().tolist())
b1.convert_to_cw(inplace=True)
print(type(b1._bound_handler), b1.get_start().tolist())
b1.convert_to_se(inplace=True)
print(type(b1._bound_handler), b1.get_start().tolist())
exit(110)

b1 = ndbounds.interpret_as_se(output, copy=False)
b2 = b1
b3 = b1.copy()

print(np.array(output).shape, b1.get_shape(), b1.get_n_dims())

print(type(b1._bound_handler), type(b2._bound_handler), type(b3._bound_handler))
b1.convert_to_cw(inplace=True)
print(type(b1._bound_handler), type(b2._bound_handler), type(b3._bound_handler))
print(b1.get_center().tolist(), b2.get_center().tolist(), b3.get_center().tolist())
print(output.tolist())
exit(110)
print((b1._bound_handler._center == b2._bound_handler._start).all())

# Should be the same
print(b1 == b2)
print(type(b1._bound_handler) == type(b2._bound_handler))
print((b1._bound_handler._bounds == b2._bound_handler._bounds).all())

exit(110)

# Should be different
print((bounds != b2._bound_handler._bounds).any())

# print(b1.get_center())
# print(b1.get_start())

b1_copy = b1.copy()

b3 = b1.add_offset([100], inplace=True)
# print(b3.get_bounds_se())
print((b3._bound_handler._bounds != b1._bound_handler._bounds).any())

print((b1_copy._bound_handler._bounds != b1._bound_handler._bounds).any())

import numpy as np
print(b2.apply_func_on_position(np.exp, inplace=False).get_bounds_cw())
print(b2.apply_func_on_position(np.exp, inplace=True).apply_func_on_position(np.exp, inplace=False).get_bounds_cw())
print(b2.apply_func_on_width(np.log, inplace=False).get_bounds_cw())
print(b2.apply_func_on_width(np.log, inplace=True).apply_func_on_width(np.log, inplace=False).get_bounds_cw())
