
import ndbounds


bounds = ndbounds.interpret_as_cw([
    [[[1,5]], [[2,9]]],
    [[[-3,8]], [[-4,4]]],
])

bounds_indexing = bounds[:, 0]
bounds_deleting = bounds.delete([0], axis=1)

print(bounds.get_shape())
print(bounds_indexing.get_bounds_cw().shape)

print(bounds_deleting.get_bounds_cw().shape)

# Should throw error
del bounds_indexing[0]

# Should throw error
bounds_indexing[0] = [[0, 0]]
