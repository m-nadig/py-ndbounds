# Copyright (C) 2021 Matthias Nadig

import numpy as np

import ndbounds


# 1D
'''
bounds = np.asarray([
    [[2, 4],],
    [[1, 9],],
    [[-3, -1],],
])
some_point = [2]
'''
# 2D
bounds = np.asarray([
    [[2, 4],
     [1, 4],],
    [[1, 9],
     [-11, 1],],
    [[-3, -1],
     [5, 70],],
])
some_point = [2, 2]

inplace = True

bounds = ndbounds.interpret_as_se(bounds)
bounds.convert_to_cw()

print('-------------------\nIs point within:\n{}'.format(bounds.check_within(some_point)))
print('-------------------\nDimensions rescaled:\n{}'.format(bounds.scale_dimensions([2, 10], inplace=inplace).get_bounds_se()))
print('-------------------\nWidths rescaled:\n{}'.format(bounds.scale_width([0.5, 1], inplace=inplace).get_bounds_se()))
print('-------------------\nOffset added:\n{}'.format(bounds.add_offset([5, 100], inplace=inplace).get_bounds_se()))
