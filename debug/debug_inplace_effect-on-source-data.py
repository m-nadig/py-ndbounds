
import numpy as np
import ndbounds


# 1D bounds in the format (start, end)
source = [
    [[[1, 5]],
     [[2, 9]]],
    [[[-3, 8]],
     [[-4, 4]]],
]

# We convert the source data here already to float, else the NdBounds-creation would lead to a copy
source = np.array(source).astype('float')
source_backup = np.copy(source)

b1 = ndbounds.interpret_as_se(source, copy=False)

print('Original array has shape {}'.format(np.array(source).shape))
print('Created {}-dimensional bounds (start, end) of shape {}'.format(b1.get_n_dims(), b1.get_shape()))

# Convert inplace
b1.convert_to_cw(inplace=True)

print('Backup of original data: {}'.format(source_backup.tolist()))
print('Array that contained the original source data: {}'.format(source.tolist()))
print('Original data modified: {}'.format((source_backup != source).any()))
