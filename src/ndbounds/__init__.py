# Copyright (C) 2021 Matthias Nadig

from ._api import as_bounds
from ._api import interpret_as_se
from ._api import interpret_as_cw
from ._api import interpret_as_s1s2e1e2
from ._api import interpret_as_s1s2w1w2

from ._api import iou

from ._api import raise_if_not_ndbounds

__version__ = '4.0.5'
__author__ = 'Matthias Nadig'
